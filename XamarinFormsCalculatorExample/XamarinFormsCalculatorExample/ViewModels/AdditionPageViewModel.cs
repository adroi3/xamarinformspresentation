﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace XamarinFormsCalculatorExample.ViewModels
{
    public class AdditionPageViewModel : INotifyPropertyChanged
    {
        private string _firstNumberAsString;
        private string _secondNumberAsString;
        private int? _result;

        public string FirstNumberAsString
        {
            get
            {
                return _firstNumberAsString;
            }

            set
            {
                _firstNumberAsString = value;
                OnPropertyChanged();
            }
        }

        public string SecondNumberAsString
        {
            get
            {
                return _secondNumberAsString;
            }

            set
            {
                _secondNumberAsString = value;
                OnPropertyChanged();
            }
        }

        public int? Result
        {
            get
            {
                return _result;
            }

            set
            {
                _result = value;
                OnPropertyChanged();
            }
        }

        public void UpdateResult()
        {
            if (!int.TryParse(FirstNumberAsString, out var firstNumber) ||
                !int.TryParse(SecondNumberAsString, out var secondNumber))
            {
                Result = null;

                return;
            }

            Result = firstNumber + secondNumber;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
