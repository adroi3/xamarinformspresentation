﻿using System;
using XamarinFormsCalculatorExample.ViewModels;

namespace XamarinFormsCalculatorExample
{
	public partial class AdditionPage
	{
		public AdditionPage()
		{
			InitializeComponent();
		}

	    private void OnResultButtonClicked(object sender, EventArgs e)
	    {
	        ((AdditionPageViewModel)BindingContext).UpdateResult();
	    }
    }
}
